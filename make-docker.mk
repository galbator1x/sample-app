docker-build-app:
	docker build services/app -f services/app/Dockerfile.production -t aishek/workshop-devops-app

docker-update-app-latest: docker-build-app
	docker push aishek/workshop-devops-app

docker-build-nginx:
	docker build services/nginx -f services/nginx/Dockerfile -t aishek/workshop-devops-nginx

docker-update-nginx-latest: docker-build-nginx
	docker push aishek/workshop-devops-nginx
