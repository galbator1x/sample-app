U := ubuntu

production-setup:
	ansible-playbook ansible/site.yml -i ansible/production -u $U -vv

production-deploy:
	ansible-playbook ansible/deploy.yml -i ansible/production -u $U -vv

production-deploy-app:
	ansible-playbook ansible/deploy.yml -i ansible/production -t app -u $U -vv
