resource "aws_cloudwatch_log_group" "devops-workshop" {
  name = "devops-workshop"

  tags {
    Environment = "production"
    Application = "app"
  }
}

resource "aws_cloudwatch_log_stream" "web" {
  name           = "web"
  log_group_name = "${aws_cloudwatch_log_group.devops-workshop.name}"
}
