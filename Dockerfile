FROM ruby:2.5

RUN gem install rails --pre

WORKDIR /services

CMD ["rails", "new", "app"]
