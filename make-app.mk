app-test:
	docker-compose run app bin/rails test

app-build:
	docker-compose build

app:
	docker-compose up

app-down:
	docker-compose down

app-bash:
	docker-compose run app bash

app-install:
	docker-compose run app bundle install

app-setup: development-setup-env app-build
	docker-compose run app bin/setup

app-db-drop:
	docker-compose run app bin/rails db:drop

app-db-prepare:
	docker-compose run app bin/rails db:create
	docker-compose run app bin/rails db:migrate
